const DEBUG = false;
const API_URL = 'http://canari.local:80/iot/api/v1.0';

const reqListener = response => {
  if(DEBUG==true){
    const content = document.getElementById("debugContent");
    let toText = response.currentTarget.response;
    content.textContent = toText;
    M.textareaAutoResize(content);
  }
};

const getPPM = () =>{
  console.log("asking ppm val...");
  const req = new XMLHttpRequest();
  req.onload = updatePPM;
  req.open("GET",API_URL+"/ppm",true);
  req.send();
}

const getConfig = () =>{
  const req = new XMLHttpRequest();
  req.onload = updateCurrent;
  req.open("GET",API_URL+"/config",true);
  req.send();
}
const reset = () =>{
  const req = new XMLHttpRequest();
  req.onload = reqListener;
  req.open("GET",API_URL+"/reset",true);
  req.send();
}
const reboot = () =>{
  const req = new XMLHttpRequest();
  req.onload = reqListener;
  req.open("GET",API_URL+"/reboot",true);
  req.send();
}
const halt = () =>{
  const req = new XMLHttpRequest();
  req.onload = reqListener;
  req.open("GET",API_URL+"/halt",true);
  req.send();
}

const scan = () =>{
  const req = new XMLHttpRequest();
  req.onload = populateList;
  req.open("GET",API_URL+"/scan",true);
  req.send();
}

const populateList = response => {
  reqListener(response);
  let dropdown = document.getElementById('wifiDropdown');
  dropdown.length = 0;
  const data = JSON.parse(response.currentTarget.response);
  //console.log(data);
  let option;
  for(i=0;i<data.length;i++){
    option = document.createElement('option');
    option.text = data[i].SSID;
    option.value = data[i].SSID;
    dropdown.add(option);
  }
}

const applyConfig = () =>{
  let wifiDropdown = document.getElementById('wifiDropdown');
  let ssidVal = wifiDropdown.options[wifiDropdown.selectedIndex].text;
  let pskVal = document.getElementById("psk").value;
  let apiDropdown = document.getElementById('apiSelect');
  let apiToUse = '';
  apiToUse=apiDropdown.options[apiDropdown.selectedIndex].text;
  let apiLat = document.getElementById('lat').value;
  let apiLong = document.getElementById('long').value;
  let apiRange = document.getElementById('range').value;
  let sensorId = document.getElementById('sensor_id').value;
  let unit = "10";
  let ledState = 0;
  let onlineState = 0;

  let jsonData = {};
  if(pskVal.length>0){
    jsonData["ssid"] = ssidVal;
    jsonData["psk"] = pskVal;
  }
  if(apiToUse.length>0)jsonData["apiToUse"] = apiToUse;
  if(sensorId.length>0)jsonData["sensorID"] = sensorId;

  if(apiLat.length>0)jsonData["apiLat"] = apiLat;
  if(apiLong.length>0)jsonData["apiLong"] = apiLong;
  if(apiRange.length>0)jsonData["apiRange"] = apiRange;

  let unit25 = document.getElementById('pm25');
  if(unit25.checked)unit = unit25.value;
  jsonData["unit"]=unit;

  if(document.getElementById('ledState').checked)ledState = 1;
  if(document.getElementById('onlineState').checked)onlineState = 1;
  jsonData["ledState"] = ledState;
  jsonData["online"] = onlineState;

  console.log('new config data:',jsonData);
  const req = new XMLHttpRequest();
  req.onload = reqListener;
  req.open("POST",API_URL+"/config",true);
  req.setRequestHeader("Content-Type","application/json");

  if(jsonData != undefined){
    let data = JSON.stringify(jsonData);
    req.send(data);
  }
}

const updateCurrent = response => {
  reqListener(response);
  let res = JSON.parse(response.currentTarget.response);
  console.log(res);
  document.getElementById("wifiDefault").textContent = res.wifi.ssid;
  document.getElementById("apiDefault").textContent = res.api.apiToUse;
  let apiSelect = document.getElementById("apiSelect");
  for(let i=1;i<apiSelect.length;i++){
    if(apiSelect[i].textContent===res.api.apiToUse)apiSelect[i].style.display="none";
  }
  console.log(apiSelect);

  let apiLat = document.getElementById('lat');
  let apiLong = document.getElementById('long');
  let sensorId = document.getElementById('sensor_id');
  let apiRange = document.getElementById('range');

  if(res.api.lat)apiLat.value = res.api.lat;
  if(res.api.long)apiLong.value = res.api.long;
  if(res.api.id)sensorId.value = res.api.id;
  if(res.api.range)apiRange.value = res.api.range;

  if(res.api.unit == 10){
    document.getElementById('pm10').checked=true;
    document.getElementById('pm25').checked=false;
  }else {
    document.getElementById('pm10').checked=false;
    document.getElementById('pm25').checked=true;
  }

  if(res.general.ledState == 1) document.getElementById('ledState').checked=true;
  else document.getElementById('ledState').checked=false;
  if(res.general.online == 1) document.getElementById('onlineState').checked=true;
  else document.getElementById('onlineState').checked=false;
}

const updatePPM = response =>{
  reqListener(response);
  let res = JSON.parse(response.currentTarget.response);
  console.log(res.ppm);
}

window.onload = () =>{
  getConfig();
  if(DEBUG==false){
    document.getElementById('debug').style.display = "none";
  }
  const scanButton = document.getElementById("wifiScan");
  scanButton.onclick = scan;
  const applyButton = document.getElementById("applyConfig");
  applyButton.onclick = applyConfig;
  const resetButton = document.getElementById("reset");
  resetButton.onclick = reset;
  const rebootButton = document.getElementById("reboot");
  rebootButton.onclick = reboot;
  const haltButton = document.getElementById("halt");
  haltButton.onclick = halt;

  getPPM();
  setInterval(getPPM, 30000);
}

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.collapsible');
  var instances = M.Collapsible.init(elems);
});
