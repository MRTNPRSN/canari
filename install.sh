#!/bin/bash
# The script configures simultaneous AP and Managed Mode Wifi on Raspberry Pi Zero W (should also work on Raspberry Pi 3)
# Licence: GPLv3
# Author: Darko Lukic <lukicdarkoo@gmail.com>
# Special thanks to: https://albeec13.github.io/2017/09/26/raspberry-pi-zero-w-simultaneous-ap-and-managed-mode-wifi/

#part of the script from http://www.uugear.com/portfolio/a-single-script-to-setup-i2c-on-your-raspberry-pi/
#custom strip from Martin Pirson

# Error management
set -o errexit
set -o pipefail
set -o nounset
usage() {
    cat 1>&2 <<EOF
Install and setup canari application and dependencies

USAGE:
    ./install.sh
EOF
    exit 0
}
# check if  is used
if [ "$(id -u)" != 0 ]; then
  echo -e "\e[91mSorry, you need to run this script with sudo\e[0m"
  exit 1
fi
AP_SSID='CANARI_SETUP'
AP_PASSPHRASE='hellocanari'
CLIENT_SSID='tempwifi'
CLIENT_PASSPHRASE='temppassword'
AP_IP='192.168.10.1'
AP_IP_BEGIN=`echo "${AP_IP}" | sed -e 's/\.[0-9]\{1,3\}$//g'`
MAC_ADDRESS="$(cat /sys/class/net/wlan0/address)"
# Install dependencies
apt -y update
apt -y upgrade
apt -y install dnsmasq dhcpcd hostapd python3-pip
echo -e "\e[93mPopulate rules.d...\e[0m"
# Populate `/etc/udev/rules.d/70-persistent-net.rules`
bash -c 'cat > /etc/udev/rules.d/70-persistent-net.rules' << EOF
SUBSYSTEM=="ieee80211", ACTION=="add|change", ATTR{macaddress}=="${MAC_ADDRESS}", KERNEL=="phy0", \
  RUN+="/sbin/iw phy phy0 interface add ap0 type __ap", \
  RUN+="/bin/ip link set ap0 address ${MAC_ADDRESS}
EOF
echo -e "\e[93mPopulate dnsmasq.conf...\e[0m"
# Populate `/etc/dnsmasq.conf`
bash -c 'cat > /etc/dnsmasq.conf' << EOF
interface=lo,ap0
no-dhcp-interface=lo,wlan0
bind-interfaces
server=8.8.8.8
domain-needed
bogus-priv
dhcp-range=${AP_IP_BEGIN}.50,${AP_IP_BEGIN}.150,12h
EOF
echo -e "\e[93mPopulate hostapd.conf...\e[0m"
# Populate `/etc/hostapd/hostapd.conf`
bash -c 'cat > /etc/hostapd/hostapd.conf' << EOF
ctrl_interface=/var/run/hostapd
ctrl_interface_group=0
interface=ap0
driver=nl80211
ssid=${AP_SSID}
hw_mode=g
channel=11
wmm_enabled=0
macaddr_acl=0
auth_algs=1
wpa=2
$([ $AP_PASSPHRASE ] && echo "wpa_passphrase=${AP_PASSPHRASE}")
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP CCMP
rsn_pairwise=CCMP
EOF
echo -e "\e[93mPopulate hostapd...\e[0m"
# Populate `/etc/default/hostapd`
bash -c 'cat > /etc/default/hostapd' << EOF
DAEMON_CONF="/etc/hostapd/hostapd.conf"
EOF
echo -e "\e[93mPopulate wpa_supplicant.conf\e[0m"
# Populate `/etc/wpa_supplicant/wpa_supplicant.conf`
bash -c 'cat > /etc/wpa_supplicant/wpa_supplicant.conf' << EOF
country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="${CLIENT_SSID}"
    $([ $CLIENT_PASSPHRASE ] && echo "psk=\"${CLIENT_PASSPHRASE}\"")
    id_str="AP1"
}
EOF
echo -e "\e[93mPopulate interfaces...\e[0m"
# Populate `/etc/network/interfaces`
bash -c 'cat > /etc/network/interfaces' << EOF
source-directory /etc/network/interfaces.d

auto lo
auto ap0
auto wlan0
iface lo inet loopback

allow-hotplug ap0
iface ap0 inet static
    address ${AP_IP}
    netmask 255.255.255.0
    hostapd /etc/hostapd/hostapd.conf

allow-hotplug wlan0
iface wlan0 inet manual
    wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
iface AP1 inet dhcp
EOF
echo -e "\e[93mPopulate start_wifi script...\e[0m"
# Populate `/bin/start_wifi.sh`
bash -c 'cat > /bin/rpi-wifi.sh' << EOF
#!/bin/bash
echo 'Starting Wifi AP and client...'
sleep 30
ifdown --force wlan0
ifdown --force ap0
ifup ap0
ifup wlan0
sysctl -w net.ipv4.ip_forward=1
iptables -t nat -A POSTROUTING -s ${AP_IP_BEGIN}.0/24 ! -d ${AP_IP_BEGIN}.0/24 -j MASQUERADE
systemctl restart dnsmasq
EOF
chmod +x /bin/rpi-wifi.sh
# enable I2C on Raspberry Pi
echo -e '\e[93mEnable I2C\e[0m'
if grep -q 'i2c-bcm2708' /etc/modules; then
  echo 'Seems i2c-bcm2708 module already exists, skip this step.'
else
  echo 'i2c-bcm2708' >> /etc/modules
fi
if grep -q 'i2c-dev' /etc/modules; then
  echo 'Seems i2c-dev module already exists, skip this step.'
else
  echo 'i2c-dev' >> /etc/modules
fi
if grep -q 'dtparam=i2c1=on' /boot/config.txt; then
  echo 'Seems i2c1 parameter already set, skip this step.'
else
  echo 'dtparam=i2c1=on' >> /boot/config.txt
fi
if grep -q 'dtparam=i2c_arm=on' /boot/config.txt; then
  echo 'Seems i2c_arm parameter already set, skip this step.'
else
  echo 'dtparam=i2c_arm=on' >> /boot/config.txt
fi
if [ -f /etc/modprobe.d/raspi-blacklist.conf ]; then
  sed -i 's/^blacklist spi-bcm2708/#blacklist spi-bcm2708/' /etc/modprobe.d/raspi-blacklist.conf
  sed -i 's/^blacklist i2c-bcm2708/#blacklist i2c-bcm2708/' /etc/modprobe.d/raspi-blacklist.conf
else
  echo 'File raspi-blacklist.conf does not exist, skip this step.'
fi
# installing python dependencies
echo -e "\e[93mInstalling canari python script's dependencies...\e[0m"
pip3 install -r /home/pi/canari/requirements.txt
#copy config files
echo -e "\e[93mCopy files in /bin\e[0m"
mkdir -p /bin/canari
cp ./default_config.json /bin/canari/default_config.json
cp ./default_config.json /bin/canari/current_config.json
cp ./app.py /bin/canari/app.py
cp ./apiRequest.py /bin/canari/apiRequest.py
cp -a ./static/ /bin/canari/
chmod +x /bin/canari/app.py
#chmod +x /bin/apiRequest.py
# Change hostname
echo -e "\e[93mChanging hostname to canari\e[0m"
echo canari | tee  /etc/hostname
sed -i -e 's/^.*hostname-setter.*$//g' /etc/hosts
echo "127.0.1.1      " canari " ### Set by hostname-setter"  | tee -a /etc/hosts
# add crontab for autostart
echo -e "\e[93mInstalling cronjob\e[0m"
echo "@reboot pi sudo /bin/rpi-wifi.sh && sudo /bin/canari/app.py&" | tee /etc/cron.d/startCanari
# Finish
echo -e "\e[92mDone! Please reboot your Raspberry Pi to apply changes...\e[0m"
echo "type sudo reboot"
