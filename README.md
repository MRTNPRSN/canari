# canari

A lamp that translate air quality data into light patterns. A project from Guillaume Slizewicz
https://studio.guillaumeslizewicz.com/post/189662102822/canari-brass-tubes-glass-globes-and-aluminium

## Updated ESP Version

You can use the ESP folder and follow [this tutorial](https://randomnerdtutorials.com/esp32-wi-fi-manager-asyncwebserver/) to understand how things work.

You can follow this [pdf](https://gitlab.com/MRTNPRSN/canari/-/raw/master/pdf/canari_instructions.pdf) for assembly, in french for the moment, otherwise you can follow the assembly part of this [instructable](https://www.instructables.com/Canari-a-Lamp-That-Transform-Air-Quality-Measureme/)

Once plugged in, you need to insert the sensor number, and the info for internet connection to the ESP32.
In order to do that, you need to connect via WIFI to the ESP network
![imagenetwork](https://gitlab.com/MRTNPRSN/canari/-/raw/master/img/wifi_1.png)

And then add the info on the page that is available at 192.168.4.1 ![image192.168.4.1](https://gitlab.com/MRTNPRSN/canari/-/raw/master/img/wifi_2.png)

You can find your sensor number on the sensor.community website. ![iamgesensor](https://gitlab.com/MRTNPRSN/canari/-/raw/master/img/sensor_community_1.png)

The ESP will then reboot and use the info you provided

## Raspberry pi Zero Version
### Hardware:

- [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/)
- [Adafruit 16-Channel PWM / Servo Bonnet](https://www.adafruit.com/product/3416)
- One WS2812B addressable LED plugged on 5V, GND and GPIO 18
- One momentary switch plugged between GND and GPIO 21
- White 3mm LEDs plugged on Signal and Ground of the PWM Bonnet

**Tested** on Raspberry Pi **Zero W** only.
(should **work** on Raspberry Pi **3B** but probably not on 3B+ & above)

### Installation instructions:

- `ssh pi@raspberrypi.local` password => raspberry
- `sudo apt update`
- `sudo apt -y install git`
- `git clone https://gitlab.com/MRTNPRSN/canari.git`
- `cd canari`
- `sudo ./install.sh`

### Usage:

- when prompted by the installer, reboot your Pi
- after a few seconds you should see the status LED slowly fading in and out in a light red color
- On your computer or mobile phone go to wiFi setup, you should be able to connect to **CANARI_SETUP** AP with password "hellocanari"
- When connected open a browser window and type: "http://canari.local"
- You will see the setuptool interface, click on SCAN WIFI to update available WiFi list
- Select your home WiFi SSID and enter your WiFi password in the textarea
- Select your options, gps coordinates and click the SET NEW CONFIG button (important, when using lufdaten/sensor.commnunity, set your id number to -1)
- You will see the status LED quickly blinking in green
- When status led stop blinking and stay on a solid blue color your done. Switch back your computer/mobile phone WiFi to your home AP

### Current State:

##### DONE:
1. Create requirements.txt
2. Create install.sh bash script for easy one command installation
3. Manage button push to call API or toggle LEDs ON/OFF (short push call API, long push > 2sec toggle LEDs)
4. Add option to toggle online/offline in setuptool
5. Add option to chose between PM10/PM2.5 in setuptool
6. Add option to toggle LEDs ON/OFF in setuptool
7. Add choice between lat/lon/range coordinates or device ID in setuptool
8. Force call to API if options change in setuptool
9. Migrate all setuptool locally and serve it with flask. Setuptool is now accessible at http://canari.local
10. Move variable NUM_LED at top of the app.py script to be easily accessible
11. Change WS2812 status led color. API call is now blinky blue, update config is now blinky green
12. Current PM value is now accessible by a http requests in setuptool
13. PM value start now a 10 ppm
14. Call to smartcitizen with sensor ID is now active
15. Cleaning the results of smartcitizen to take only the sensors that have been updated less than 5 days from the date of the call

##### TODO:

1. Change LEDs animations with a "particle system"
2. Update Irceline API call in apiRequest.py (currently doing nothing with the retrieved datas)
3. Display current PM value in setuptool interface (It's already the case in the console, if you want to try on Chrome open the developer tool console)
4. Reset to default is not active. Should clean some of the entry for this one...

##### NICE TO HAVE:

1. Display the state of the device, stop requests if device offline, notify user if device not found or restarting,...
2. Onboarding more user-friendly
3. How to connect multiple PWM hat together

##### KNOWN ISSUES:

1. A bug with first call to wifi scan after a boot or reboot. Juste re-click on scan wifi to get the updated list

###### pre-installed image:
