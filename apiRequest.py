#API REQUEST MODULE
import requests,json
from datetime import datetime

luftdatenDom = 'https://data.sensor.community/airrohr/v1/'
smartCitizenDom = 'https://api.smartcitizen.me/v0/'
ircelineDom = 'https://geo.irceline.be/sos/api/v1/'

def callLuftdaten(lat,lon,dist,unit):
    type = 'P1'
    ttype = "PM10"
    if(unit == "2.5"):
        type = 'P2'
        ttype = "PM2.5"

    call = luftdatenDom+'filter/area='+str(lat)+','+str(lon)+','+str(dist)
    print(call)
    pm = 10
    try:
        r=requests.get(url=call)
        response = r.json()
        if(response):
            totalP = 0
            countP = 0
            print("sensor list length: "+str(len(response)))
            for d in response:
                for s in d.get('sensordatavalues'):
                    if(s.get('value_type')== type):
                        val = s.get('value')
                        print(val)
                        if(val and float(val)<200):
                            countP += 1
                            totalP += float(val)
            pm = totalP/countP
            print("average",ttype,"val:"+str(pm))
        else: print("bad request or api did not respond")
    except Exception as e:
        pass
        print(str(e))
    finally: return pm

def callLuftdatenID(id,unit):
    type = 'P1'
    ttype = "PM10"
    if(str(unit) == "2.5"):
        type = 'P2'
        ttype = "PM2.5"

    call = luftdatenDom+'sensor/'+str(id)+'/'
    print(call)
    pm = 10
    try:
        r=requests.get(url=call)
        response = r.json()
        print(response[0])
        for s in response[0].get('sensordatavalues'):
            if(s.get('value_type')== type):
                val = s.get('value')
                print(val)
                if(val and float(val)<200):
                    pm = val
        print("average",ttype,"val:"+str(pm))
    except Exception as e:
        pass
        print(str(e))
    finally: return pm

def callSmartcitizen(lat,lon,dist,unit):
    # find device type by ID
    # 87 = pm2.5
    # 88 = pm10
    # 89 = pm1
    type = 88
    ttype = "PM10"
    if(str(unit) == "2.5"):
        type = 87
        ttype = "PM2.5"
    call = smartCitizenDom+'devices?near='+str(lat)+','+str(lon)+'&q[state_eq]=has_published'
    print(call)
    pm = 10
    try:
        r = requests.get(url=call)
        response = r.json()
        if(response):
            totalP = 0
            countP = 0
            dateNow = datetime.now()
            print("sensor list length: "+str(len(response)))
            for d in response:
                deviceData = d.get('data')
                for s in deviceData.get('sensors'):
                    if(s.get('id')==type):
                        val = s.get('value')
                        lastReading = d.get('last_reading_at')
                        toDateTime = datetime.strptime(lastReading,'%Y-%m-%dT%H:%M:%SZ')
                        deltaDays = (dateNow-toDateTime).days
                        if(val and deltaDays < 5):
                            print(val,d.get('name'))
                            countP += 1
                            totalP += float(val)
            pm = totalP/countP
            print("average",ttype,"val:"+str(pm))
        else: print("bad request or api did not respond")
    except Exception as e:
        pass
        print(str(e))
    finally: return pm

def callSmartcitizenID(unit,id='-1'):
    # find device type by ID
    # 87 = pm2.5
    # 88 = pm10
    # 89 = pm1
    type = 88
    ttype = "PM10"
    if(str(unit) == "2.5"):
        type = 87
        ttype = "PM2.5"
    call = smartCitizenDom+'devices/'+str(id)
    print(call)
    pm = 10
    try:
        r = requests.get(url=call)
        response = r.json()
        if(response):
            deviceData = response.get('data')
            for s in deviceData.get('sensors'):
                if(s.get('id')==type):
                    val = s.get('value')
                    print(val,response.get('name'),response.get('last_reading_at'))
            pm = val
            print(ttype,"val:"+str(pm))
        else: print("bad request or api did not respond")
    except Exception as e:
        pass
        print(str(e))
    finally: return pm

def callIrceline(lat,lon,dist,unit):
    call = ircelineDom+'stations?near={"center": {"type": "Point","coordinates": ['+lon+','+lat+']},"radius":'+dist+'}'
    pm = 10
    try:
        r = requests.get(url=call)
        response = r.json()
        print(response)
        return 1
    except Exception as e:
        pass
        print(str(e))
    finally: return pm