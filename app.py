#!/usr/bin/python3
import threading, atexit, time, math, socket, os, json, shutil, random
from flask import Flask, jsonify, make_response, request
from flask_cors import CORS
from wifi import Cell,Scheme
import RPi.GPIO as GPIO
import board, busio, adafruit_pca9685,neopixel,digitalio
import apiRequest

NUM_LED = 7
API_CHECK_DELAY_S = 120
NET_CHECK_DELAY_S = 30

# -- Flask variables -- #
HOST = '0.0.0.0'
PORT = 80
API_URL = '/iot/api/v1.0'
config = None
wifi = None
api = None
general = None

# -- init flask app and cors policy modules -- #
app = Flask(__name__)
CORS(app)

# -- IO threads and variables -- #
i2c = busio.I2C(board.SCL,board.SDA)
hat = adafruit_pca9685.PCA9685(i2c)
hat.frequency = 60

backToUsine = None
rebootCMD = None
haltCMD = None

updatingConfig = False
online = False
forceCheck = False

OFFLINE = False
STATE = 0
PPM = 10

dirname = os.path.dirname(__file__)
defaultConfigPath = os.path.join(dirname,"default_config.json")
currentConfigPath = os.path.join(dirname,"current_config.json")
print(defaultConfigPath)
print(currentConfigPath)

# -- get current config from json file -- #
with open(currentConfigPath) as json_file:
    config = json.load(json_file)
    wifi = config['wifi']
    api = config['api']
    general = config['general']
json_file.close()

# ------------------------------ #
# ----- DEFINE FLASK ROUTES ---- #
# ------------------------------ #
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error':'Not found'}),404)

@app.route('/')
def root():
    print("root request")
    return app.send_static_file('index.html')

@app.route(API_URL+'/config',methods=['GET'])
def get_config():
    return jsonify(config)

@app.route(API_URL+'/reboot',methods=['GET'])
def reboot():
    global STATE
    STATE = 5
    print("reboot in 3sec...")
    time.sleep(3)
    rebootCMD()
    return jsonify({'reboot':True})

@app.route(API_URL+'/halt',methods=['GET'])
def halt():
    global STATE
    STATE = 5
    print("shutdown in 3sec...")
    time.sleep(3)
    haltCMD()
    return jsonify({'halt':True})

@app.route(API_URL+'/reset',methods=['GET'])
def reset():
    backToUsine()
    restartWifiCMD()
    return jsonify(config)

@app.route(API_URL+'/scan',methods=['GET'])
def scanForCells():
    global STATE
    currentState = STATE
    STATE = 4
    cells = []
    networks = []
    try:
        print("scan wifi AP")
        cells = Cell.all('wlan0')
        for c in cells:
            item = {"SSID":c.ssid,"SIGNAL":c.signal}
            networks.append(item)
    except Exception as e:
        print("error - rescanning now")
        scanForCells()
    time.sleep(2.5)
    STATE = currentState
    return jsonify(networks)

@app.route(API_URL+'/ppm',methods=['GET'])
def getPPM():
    print(PPM)
    return jsonify({'ppm': PPM})

@app.route(API_URL+'/config',methods=['POST'])
def update_config():
    global forceCheck
    global STATE
    lastState = STATE
    STATE = 2
    lastWiFiSSID = wifi['ssid']
    lastWiFiPSK = wifi['psk']
    wifi['ssid'] = request.json.get('ssid',wifi['ssid'])
    wifi['psk'] = request.json.get('psk',wifi['psk'])
    wifi['country'] = request.json.get('country',wifi['country'])
    print(wifi['country'],wifi['ssid'],wifi['psk'])

    api['apiToUse'] = request.json.get('apiToUse',api['apiToUse'])
    api['lat'] = request.json.get('apiLat',api['lat'])
    api['long'] = request.json.get('apiLong',api['long'])
    api['id'] = request.json.get('sensorID',api['id'])
    api['unit'] = request.json.get('unit',api['unit'])
    api['range'] = request.json.get('apiRange',api['range'])
    print(api['apiToUse'],api['lat'],api['long'],api['id'],api['unit'],api['range'])

    general['ledState'] = request.json.get('ledState',general['ledState'])
    general['online'] = request.json.get('online',general['online'])
    general['deviceName'] = request.json.get('deviceName',general['deviceName'])
    print(general['ledState'],general['online'],general['deviceName'])

    with open(currentConfigPath,'w') as out_file:json.dump(config,out_file,indent=4)

    if(lastWiFiSSID!=wifi['ssid'] or lastWiFiPSK!=wifi['psk']):
        populateWpasupplicant(wifi['country'],wifi['ssid'],wifi['psk'])
        restartWifiCMD()

    time.sleep(2.5)
    STATE = lastState
    forceCheck = True
    return jsonify(config)

# ---------------------------------- #
# --- File Manipulation & Helper --- #
# ---------------------------------- #
def backToUsine():
    print('back to default setup')
    with open(defaultConfigPath) as json_file:
        defaultConfig = json.load(json_file)
        defaultWifi = defaultConfig['wifi']
    json_file.close()

    populateWpasupplicant(defaultWifi['country'],defaultWifi['apName'],defaultWifi['apPsk'])

    wifi['deviceName'] = defaultWifi['deviceName']
    wifi['ssid'] = defaultWifi['ssid']
    wifi['psk'] = defaultWifi['psk']
    wifi['apName'] = defaultWifi['apName']
    wifi['apPsk'] = defaultWifi['apPsk']
    wifi['country'] = defaultWifi['country']
    with open(currentConfigPath,'w') as out_file:json.dump(config,out_file,indent=4)

def populateWpasupplicant(country,ssid,psk):
    content = [
        'country='+country,
        'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev',
        'update_config=1\n',
        'network={',
        'ssid="'+ssid+'"',
        'psk="'+psk+'"',
        'id_str="AP1"',
        '}'
    ]
    wpasupplicantPath = os.path.abspath('/etc/wpa_supplicant/wpa_supplicant.conf')
    with open(wpasupplicantPath,'w') as wpa_file:
        for item in content: print(item,file=wpa_file)
    wpa_file.close()

def restartWifiCMD():
    global STATE
    STATE = 2
    global updatingConfig
    updatingConfig = True
    os.system("/bin/rpi-wifi.sh")
    updatingConfig = False

def haltCMD():
    global STATE
    STATE = 5
    time.sleep(1)
    os.system("sudo halt")

def rebootCMD():
    global STATE
    STATE = 5
    time.sleep(1)
    os.system("sudo reboot")

# ---------------------------------- #
# -- IO MANAGER FUNCTIONS & CLASS -- #
# ---------------------------------- #
def manageIO():
    global NUM_LED
    lastDebugTick = 0
    #init pwm hat
    i2c = busio.I2C(board.SCL,board.SDA)
    hat = adafruit_pca9685.PCA9685(i2c)
    hat.frequency = 60
    #declare needle display
    NEEDLE_DISPLAY = hat.channels[15]
    #declare leds
    leds = []
    ledTimings = []
    #init status led
    STATUS_LED_PIN = board.D18
    ORDER = neopixel.GRB
    BRIGHTNESS = .4
    statusLed = neopixel.NeoPixel(STATUS_LED_PIN, 1, brightness=BRIGHTNESS, auto_write=False,pixel_order=ORDER)
    global statusBright
    statusBright = 0
    #init button
    BUTTON_PIN = board.D21
    button = digitalio.DigitalInOut(BUTTON_PIN)
    button.direction = digitalio.Direction.INPUT
    button.pull = digitalio.Pull.UP
    global lastButtonState
    lastButtonState = button.value
    global pushStartTime
    pushStartTime = 0
    global pushTime
    pushTime = 0
    global btnFlag
    btnFlag = False
    #declare variables for delta time calculation
    global now
    global then
    now = 0
    then = 0
    global updatingConfig
    # ---- LIGHT AND LEDS ANIMATIONS ---- #
    #this provide the delta time between two update
    #use it as time reference for time dependent animations
    def getDelta():
        delta = (now - then)*1000
        return delta
    #self-talking name
    def initLeds():
        for i in range(NUM_LED):
            leds.append(hat.channels[i])
            ledTimings.append(random.randint(0,1000))
    #self-talking name
    def updateLeds(ppm):
        #!TODO convert ppm to speed value
        #!TODO update led display function
        norm = ppm/80
        speed = 1+(norm*10) #?????
        for l in range(len(leds)):
            b = speed*getDelta()
            ledTimings[l] += b
            bright = (0xffff/2)+(math.sin(ledTimings[l])*(0xffff/2))
            leds[l].duty_cycle = int(bright)
    def allLedsToBlack():
        for l in range(len(leds)):leds[l].duty_cycle = 0
    def switchAllOff():
        allLedsToBlack()
        statusLed.fill((0,0,0))
        statusLed.brightness = 0
        statusLed.show()
    #state machine for the different status led behaviours
    def updateStatusLed(state):
        if(state==0):
            #not configured or no internet connection
            statusFade((242,100,110),10)
        elif(state==1):
            #configured and internet connection ok
            statusLed.brightness = BRIGHTNESS
            statusLed.fill((16,242,161))
        elif(state==2):
            #updating configuration
            statusFade((0,201,52),50)
        elif(state==3):
            #calling API
            statusFade((16,242,161),30)
        elif(state==4):
            #wifi scan
            statusFade((255,255,255),15)
        elif(state==5):
            #reboot or shutdown
            statusFade((255,10,10),50)
        elif(state==-1):
            statusLed.fill((0,0,0))

    #helper function to fade STATUS LED
    def statusFade(color,speed):
        global statusBright
        statusLed.fill(color)
        b = speed*getDelta()
        statusBright += b
        bright = (BRIGHTNESS/2)+math.sin(statusBright)*(BRIGHTNESS/2)
        if(button.value):statusLed.brightness = bright

    # ---- BUTTON INTERACTIONS ----- #
    def updateButton():
        global forceCheck
        global lastButtonState
        global pushStartTime
        global btnFlag
        global pushTime
        if(button.value != lastButtonState):
            if(button.value):
                btnFlag = False
                if(pushTime<1):forceCheck = True
            else:
                pushStartTime = time.time()
            lastButtonState = button.value
        if not(button.value):
            pushTime = (time.time()-pushStartTime)
            statusLed.brightness = BRIGHTNESS
            statusLed.fill((252,3,161))
            if(pushTime>2 and btnFlag==False):
                general['ledState'] = not general['ledState']
                btnFlag = True


    # ---- NEEDLE DISPLAY HELPER ---- #
    def updateNeedleDisplay():
        #!TODO DEBUG only
        needleVal = (0xffff/2)+math.sin(time.time())*(0xffff/2)
        NEEDLE_DISPLAY.duty_cycle = int(needleVal)

    initLeds()
    while True:
        try:
            #UPDATE LEDS & BUTTON STATE
            now = time.time()
            if(STATE!=5):
                if(general['ledState']==1):updateLeds(PPM)
                else: allLedsToBlack()
                updateStatusLed(STATE)
                updateButton()
                statusLed.show()
            else:
                print("exit programme")
                switchAllOff()
                exit()
            then = time.time()

        except Exception as e:
            print ('exception in IO Thread: '+str(e))
            return

#_______________________________________#
# ______ NETWORK CHECK FUNCTION  ______ #
#_______________________________________#
def checkNetwork():
    global OFFLINE
    global STATE
    global online
    lastTimeNetCheck = 0

    while True:
        if(OFFLINE):online = False
        else:
            if(time.time()-lastTimeNetCheck > NET_CHECK_DELAY_S):
                print("checking network...")
                try:
                    host = socket.gethostbyname('www.google.com')
                    s = socket.create_connection((host,80),2)
                    s.close()
                    STATE = 1
                    print("online")
                    online = True
                except:
                    pass
                    STATE = 0
                    print("offline")
                    online = False
                finally:
                    lastTimeNetCheck = time.time()
#_______________________________________#
# ________ API CHECK FUNCTION  ________ #
#_______________________________________#
def checkApi():
    global PPM
    global online
    global forceCheck
    lastTimeAPICheck = 0

    def callToApi():
        global STATE
        currentState = STATE
        STATE = 3
        global PPM
        time.sleep(0.5)
        print('\n*------------------------------*')
        print("checking:"+api['apiToUse']+" @lat:"+api['lat']+";lon:"+api['long']+" unit:"+api['unit']+" in range:"+api['range'])
        if(api['apiToUse']=="Lufdaten"):
            if(api['id'] == '-1'):PPM = apiRequest.callLuftdaten(api['lat'],api['long'],api['range'],api['unit'])
            else: PPM = apiRequest.callLuftdatenID(api['unit'],api['id'])
        elif(api['apiToUse']=="Smart Citizen"):
            if(api['id']=='-1'):PPM = apiRequest.callSmartcitizen(api['lat'],api['long'],api['range'],api['unit'])
            else: PPM = apiRequest.callSmartcitizenID(api['unit'],api['id'])
        #elif(api['apiToUse']=='Irceline'):PPM = apiRequest.callIrceline(api['lat'],api['long'],api['range'],api['unit'])
        time.sleep(1)
        print('*------------------------------*\n')
        STATE = currentState

    while True:
        if(online and forceCheck):
            callToApi()
            forceCheck = False
        if(online and time.time()-lastTimeAPICheck > API_CHECK_DELAY_S):
            try:
                callToApi()
            except Exception as e:
                pass
                print(str(e))
            finally:
                lastTimeAPICheck = time.time()

# ---------------------------------- #
# ----- STARTING THREADS AND MAIN FUNCTION ----- #
# ---------------------------------- #
IOThread = threading.Thread(target = manageIO)
IOThread.start()

apiThread = threading.Thread(target=checkApi)
apiThread.start()

netThread = threading.Thread(target=checkNetwork)
netThread.start()

if __name__ == '__main__':
    try:
        app.run(host=HOST,port=PORT)
    except(KeyboardInterrupt,SystemExit):
        print('Interrupt in main thread')
        sys.exit()
