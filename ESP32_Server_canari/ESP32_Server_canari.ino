/*********
  Server script created by Guillaume Slizewicz based on a tutorial by Rui Santos
  Complete instructions at https://RandomNerdTutorials.com/esp32-wi-fi-manager-asyncwebserver/

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*********/

#include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>
#include "SPIFFS.h"
#include <HTTPClient.h>
#include <ArduinoJson.h>

/////////////////////////
// FROM OTHER SCRIPT ////
/////////////////////////

//String for storing server response
String response = "";

//JSON document
DynamicJsonDocument doc(2048);
int value_pol = 1;
int lap = 0;
int i;
float R;

//The API URL
//String request = "https://data.sensor.community/airrohr/v1/sensor/71858/";
String request = "https://data.sensor.community/airrohr/v1/sensor/";

bool connection = false;

// the number of the LED pin


const int ledPin[] = {5, 16, 17, 18, 19};

const int led_number = 5;


// setting PWM properties
const int freq = 5000;
const int ledChannel[] = {0, 1, 2, 3, 4};

const int resolution = 8;
int dutyCycle[] = {0, 75, 125, 200, 250};
int deltaCycle[] = {1, 1, 1, 1, 1};
int norm = 1;
int speed_pol;
int brightness;
int dutyAngle[] = {0, 0, 0, 0, 0};
int pause_loop;
int n_loop[] = {0, 0, 0, 0, 0};

/////////////////
/////////////////

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Search for parameter in HTTP POST request
const char* PARAM_INPUT_1 = "ssid";
const char* PARAM_INPUT_2 = "pass";
const char* PARAM_INPUT_3 = "sensor_id";



//Variables to save values from HTML form
String ssid;
String pass;
String sensor_id;
String gateway;

// File paths to save input values permanently
const char* ssidPath = "/ssid.txt";
const char* passPath = "/pass.txt";
const char* sensor_idPath = "/sensor_id.txt";
const char* gatewayPath = "/gateway.txt";

//IPAddress localIP;
//IPAddress localIP(192, 168, 0, 220); // hardcoded

// Set your Gateway IP address
IPAddress localGateway;
//IPAddress localGateway(192, 168, 1, 1); //hardcoded
IPAddress subnet(255, 255, 0, 0);

// Timer variables
unsigned long previousMillis = 0;
const long interval = 10000;  // interval to wait for Wi-Fi connection (milliseconds)

// Initialize SPIFFS
void initSPIFFS() {
  if (!SPIFFS.begin(true)) {
    Serial.println("An error has occurred while mounting SPIFFS");
  }
  Serial.println("SPIFFS mounted successfully");
}

// Read File from SPIFFS
String readFile(fs::FS &fs, const char * path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return String();
  }

  String fileContent;
  while (file.available()) {
    fileContent = file.readStringUntil('\n');
    break;
  }
  return fileContent;
}

// Write file to SPIFFS
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\r\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("- failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("- file written");
  } else {
    Serial.println("- frite failed");
  }
}

// Initialize WiFi
bool initWiFi() {
  if (ssid == "" || sensor_id == "") {
    Serial.println("Undefined SSID or IP address.");
    return false;
  }

  WiFi.mode(WIFI_STA);
  //localIP.fromString(sensor_id.c_str());
  //localGateway.fromString(gateway.c_str());


  //  if (!WiFi.config(localIP, localGateway, subnet)) {
  //    Serial.println("STA Failed to configure");
  //    return false;
  //  }
  WiFi.begin(ssid.c_str(), pass.c_str());
  Serial.println("Connecting to WiFi...");

  unsigned long currentMillis = millis();
  previousMillis = currentMillis;

  while (WiFi.status() != WL_CONNECTED) {
    currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
      Serial.println("Failed to connect.");
      connection = false;
      return false;
    }
  }

  Serial.print("WiFi connected with IP: ");
  connection = true;
  Serial.println(WiFi.localIP());
  return true;
}

// Replaces placeholder with LED state value
String processor(const String& var) {
  if (var == "STATE") {
    return String();
  }
}


void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);
  R = (260 * log10(2)) / (log10(255));

  for (i = 0; i < led_number; i++) {
    // configure LED PWM functionalitites
    ledcSetup(ledChannel[i], freq, resolution);
    // attach the channel to the GPIO to be controlled
    ledcAttachPin(ledPin[i], ledChannel[i]);
  }

  initSPIFFS();

  // Load values saved in SPIFFS
  ssid = readFile(SPIFFS, ssidPath);
  pass = readFile(SPIFFS, passPath);
  sensor_id = readFile(SPIFFS, sensor_idPath);
  gateway = readFile (SPIFFS, gatewayPath);
  Serial.println(ssid);
  Serial.println(pass);
  Serial.println(sensor_id);
  //Serial.println(gateway);

  if (initWiFi()) {
    Serial.println("connection initiated");
  }
  else {
    // Connect to Wi-Fi network with SSID and password

    Serial.println("Setting AP (Access Point)");
    // NULL sets an open Access Point
    WiFi.softAP("CANARI_LAMP_setup", NULL);

    IPAddress IP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(IP);

    // Web Server Root URL
    server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
      request->send(SPIFFS, "/wifimanager.html", "text/html");
    });

    server.serveStatic("/", SPIFFS, "/");

    server.on("/", HTTP_POST, [](AsyncWebServerRequest * request) {
      int params = request->params();
      for (int i = 0; i < params; i++) {
        AsyncWebParameter* p = request->getParam(i);
        if (p->isPost()) {
          // HTTP POST ssid value
          if (p->name() == PARAM_INPUT_1) {
            ssid = p->value().c_str();
            Serial.print("SSID set to: ");
            Serial.println(ssid);
            // Write file to save value
            writeFile(SPIFFS, ssidPath, ssid.c_str());
          }
          // HTTP POST pass value
          if (p->name() == PARAM_INPUT_2) {
            pass = p->value().c_str();
            Serial.print("Password set to: ");
            Serial.println(pass);
            // Write file to save value
            writeFile(SPIFFS, passPath, pass.c_str());
          }
          // HTTP POST ip value
          if (p->name() == PARAM_INPUT_3) {
            sensor_id = p->value().c_str();
            Serial.print("Sensor id set to: ");
            Serial.println(sensor_id);
            // Write file to save value
            writeFile(SPIFFS, sensor_idPath, sensor_id.c_str());
          }
          // HTTP POST gateway value
          //          if (p->name() == PARAM_INPUT_4) {
          //            gateway = p->value().c_str();
          //            Serial.print("Gateway set to: ");
          //            Serial.println(gateway);
          //            // Write file to save value
          //            writeFile(SPIFFS, gatewayPath, gateway.c_str());
          //          }
          //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
      }
      request->send(200, "text/plain", "Done. ESP will restart. " );
      delay(3000);
      ESP.restart();
    });
    server.begin();
  }
}

void loop() {
  if (lap % 5000 == 0) {
    if (connection) {
      //Initiate HTTP client
      HTTPClient http;
      //Start the request
      http.begin(request + sensor_id + "/");
      Serial.println("http =");
      Serial.println(request + sensor_id + "/");
      //Use HTTP GET request
      http.GET();
      //Response from server
      response = http.getString();
      Serial.println(response);
      //Parse JSON, read error if any
      DeserializationError error = deserializeJson(doc, response);
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
        lap=4500;
        delay(2000);
        return;
      }
      else{
        lap = 0;
      }
      //Print parsed value on Serial Monitor
      Serial.println(doc[0]["sensordatavalues"][1]["value"].as<char*>());
      if (doc[0]["sensordatavalues"][1].containsKey("value")){
      value_pol = doc[0]["sensordatavalues"][1]["value"];
      Serial.println(value_pol);
      lap = 0;
      }
      else{
        Serial.println("no value");
        lap=4500;
      }
      //Close connection
      http.end();
      delay(2000);
      if (value_pol == 0) {
        value_pol = 1;
      }
      //value_pol=100;
      norm = value_pol / 8;
      speed_pol = 1 + norm ;
      pause_loop = 2000 / value_pol;
    }
  }



  // changing the LED brightness with PWM

  for (i = 0; i < led_number; i++) {
    if (dutyCycle[i] <= 0) {
      if (n_loop[i] <= pause_loop) {
        deltaCycle[i] = 0;
        dutyCycle[i] = 0;
        n_loop[i] = n_loop[i] + 1;
      }
      else {
        deltaCycle[i] = 1;
        dutyCycle[i] = 0;
      }
    }
    else if (dutyCycle[i] >= 255) {
      deltaCycle[i] = -1;
      dutyCycle[i] = 255;
      n_loop[i] = random(50);
    }
    dutyCycle[i] = dutyCycle[i] + (deltaCycle[i] * speed_pol);
    //    dutyAngle[i] = map(dutyCycle[i], 1, 255, 0, 360);
    brightness = pow (2, (dutyCycle[i] / R)) - 1;
    //    dutyCycle[i] = bright;

    ledcWrite(ledChannel[i], brightness);


  }
  delay(300 / value_pol);

  lap += 1;
  Serial.println("lap");
  Serial.println(lap);
  Serial.println("dutyCycle[0]");
  Serial.println(dutyCycle[0]);
}
